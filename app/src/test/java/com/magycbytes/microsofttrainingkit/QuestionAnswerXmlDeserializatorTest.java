package com.magycbytes.microsofttrainingkit;

import org.junit.Test;

/**
 * Created by sir Gregpry on 2/19/2017.
 */
public class QuestionAnswerXmlDeserializatorTest {

    @Test
    public void normal() {
        QuestionAnswerXmlDeserializator.deserialize("<answers><answer><answerid><![CDATA[461P_1.1_01-1]]></answerid><mattext>SELECT . . . INTO</mattext><score>1</score><iscorrect>True</iscorrect></answer><answer><answerid><![CDATA[461P_1.1_01-2]]></answerid><mattext>CREATE TABLE</mattext><score>0</score><iscorrect>False</iscorrect></answer><answer><answerid><![CDATA[461P_1.1_01-3]]></answerid><mattext>ALTER TABLE</mattext><score>0</score><iscorrect>False</iscorrect></answer><answer><answerid><![CDATA[461P_1.1_01-4]]></answerid><mattext>DROP TABLE</mattext><score>0</score><iscorrect>False</iscorrect></answer></answers>");
    }

}