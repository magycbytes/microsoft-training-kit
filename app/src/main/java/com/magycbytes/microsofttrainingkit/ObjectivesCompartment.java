package com.magycbytes.microsofttrainingkit;

import com.google.gson.Gson;
import com.magycbytes.microsofttrainingkit.models.Objective;
import com.magycbytes.microsofttrainingkit.models.TrainingContent;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sir Gregory on 2/10/2017.
 */

public class ObjectivesCompartment extends CompartmentParser {

    private List<Objective> mObjectives = new ArrayList<>();

    public ObjectivesCompartment(BufferedReader bufferedReader) {
        super(bufferedReader);
    }

    @Override
    public String getSerializedResponse() {
        return new Gson().toJson(mObjectives);
    }

    @Override
    public void saveLineValue(String fileLine) {
        String[] values = getSplitValues();

        Objective newObjective = new Objective();
        newObjective.setId(values[0]);
        newObjective.setName(values[2]);

        mObjectives.add(newObjective);
    }

    @Override
    public void visit(TrainingContent trainingContent) {
        trainingContent.setObjectives(mObjectives);
    }
}
