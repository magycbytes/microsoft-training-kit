package com.magycbytes.microsofttrainingkit;

import com.magycbytes.microsofttrainingkit.models.TrainingContent;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Created by sir Gregory on 2/9/2017.
 */

public abstract class CompartmentParser {

    protected BufferedReader mBufferedReader;
    private String mCurrentLine;
    protected String mCurrentValue;

    CompartmentParser(BufferedReader bufferedReader) {
        mBufferedReader = bufferedReader;
    }

    protected String getNextLine() throws IOException {
        mCurrentLine = mBufferedReader.readLine();
        if (mCurrentLine == null) return null;

        if (mCurrentLine.startsWith("[")) {
            return null;
        }
        mCurrentValue = mCurrentLine.substring(mCurrentLine.indexOf("=") + 1);
        mCurrentLine = mCurrentLine.substring(0, mCurrentLine.indexOf("="));
        return mCurrentLine;
    }

    public String getCurrentLine() {
        return mCurrentLine;
    }

    public void parse() throws IOException {
        do {
            String fileLine = getNextLine();
            if (fileLine == null) return;

            saveLineValue(fileLine);
        } while (true);
    }

    protected String[] getSplitValues() {
        return mCurrentValue.split(Pattern.quote("<z|>"));
    }

    public abstract String getSerializedResponse() ;

    public abstract void saveLineValue(String fileLine);

    public abstract void visit(TrainingContent trainingContent);
}
