package com.magycbytes.microsofttrainingkit.customViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.magycbytes.microsofttrainingkit.R;

/**
 * Created by Alex on 22/02/2017.
 */

public class ButtonWithImage extends FrameLayout {

    private ImageView mImage;
    private TextView mText;
    private View mBackground;

    public ButtonWithImage(@NonNull Context context) {
        super(context);
        inflateLayout();
        init(null, 0);
    }

    public ButtonWithImage(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflateLayout();
        init(attrs, 0);
    }

    public ButtonWithImage(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateLayout();
        init(attrs, defStyleAttr);
    }

    private void inflateLayout() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_button_with_image, this, true);

        mImage = (ImageView) view.findViewById(R.id.buttonImage);
        mText = (TextView) view.findViewById(R.id.buttonText);
        mBackground = view.findViewById(R.id.rootLayout);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.ButtonWithImage, defStyle, 0);

        if (a.hasValue(R.styleable.ButtonWithImage_ButtonBackground)) {
            mBackground.setBackgroundColor(a.getColor(R.styleable.ButtonWithImage_ButtonBackground, 0));
        }
        if (a.hasValue(R.styleable.ButtonWithImage_ButtonImage)) {
            mImage.setImageResource(a.getResourceId(R.styleable.ButtonWithImage_ButtonImage, 0));
        }
        if (a.hasValue(R.styleable.ButtonWithImage_ButtonText)) {
            mText.setText(a.getString(R.styleable.ButtonWithImage_ButtonText));
        }
        if (a.hasValue(R.styleable.ButtonWithImage_ButtonTextColor)) {
            mText.setTextColor(a.getColor(R.styleable.ButtonWithImage_ButtonTextColor, 0));
        }

        a.recycle();
    }
}
