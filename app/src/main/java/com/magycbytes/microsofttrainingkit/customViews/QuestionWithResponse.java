package com.magycbytes.microsofttrainingkit.customViews;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.magycbytes.microsofttrainingkit.R;

/**
 * Created by Alex on 15/03/2017.
 */

public class QuestionWithResponse extends FrameLayout {

    private RadioButton mQuestion;
    private TextView mAnswer;


    public QuestionWithResponse(@NonNull Context context) {
        super(context);
        inflateLayout();
    }

    public QuestionWithResponse(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflateLayout();
    }

    public QuestionWithResponse(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateLayout();
    }

    public void show(String question, String answer) {
        mQuestion.setText(question);
        mAnswer.setText(answer);
    }

    private void inflateLayout() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_radiobutton_question_with_answer, this, true);

        mQuestion = (RadioButton) view.findViewById(R.id.responseOption);
        mAnswer = (TextView) view.findViewById(R.id.responseExplanation);
    }
}
