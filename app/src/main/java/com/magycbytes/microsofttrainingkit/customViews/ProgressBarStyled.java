package com.magycbytes.microsofttrainingkit.customViews;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.magycbytes.microsofttrainingkit.R;

import java.util.Locale;

/**
 * Created by Alex on 24/02/2017.
 */

public class ProgressBarStyled extends FrameLayout {

    private TextView mProgressText;
    private ProgressBar mProgressIndicator;

    public ProgressBarStyled(@NonNull Context context) {
        super(context);
        inflateLayout();
    }

    public ProgressBarStyled(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflateLayout();
    }

    public ProgressBarStyled(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflateLayout();
    }

    public void showProgress(int progressValue) {
        mProgressText.setText(String.format(Locale.getDefault(), "%d%%", progressValue));
        mProgressIndicator.setProgress(progressValue);
    }

    private void inflateLayout() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_progress_bar_styled, this, true);

        mProgressIndicator = (ProgressBar) view.findViewById(R.id.progress_bar);
        mProgressText = (TextView) view.findViewById(R.id.progress_value);
    }
}
