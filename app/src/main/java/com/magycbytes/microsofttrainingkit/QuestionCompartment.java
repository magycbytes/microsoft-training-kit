package com.magycbytes.microsofttrainingkit;

import com.google.gson.Gson;
import com.magycbytes.microsofttrainingkit.models.Question;
import com.magycbytes.microsofttrainingkit.models.TrainingContent;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by sir Gregpry on 2/19/2017.
 */

public class QuestionCompartment extends CompartmentParser {

    private List<Question> mQuestions = new ArrayList<>();

    public QuestionCompartment(BufferedReader bufferedReader) {
        super(bufferedReader);
    }

    @Override
    public String getSerializedResponse() {
        return new Gson().toJson(mQuestions);
    }

    @Override
    public void saveLineValue(String fileLine) {
        String[] values = getSplitValues();

        Question question = new Question();
        question.setId(values[0]);
        question.setObjectiveId(values[1]);
        question.setText(values[2].replaceAll(Pattern.quote("<zEOL>"), "\n"));
        question.setExplanation(values[3].replaceAll(Pattern.quote("<zEOL>"), "\n"));
        question.setAnswers(QuestionAnswerXmlDeserializator.deserialize(values[values.length - 1]));

        mQuestions.add(question);
    }

    @Override
    public void visit(TrainingContent trainingContent) {
        trainingContent.setQuestions(mQuestions);
    }
}
