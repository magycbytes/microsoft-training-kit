package com.magycbytes.microsofttrainingkit.models;

/**
 * Created by sir Gregpry on 2/19/2017.
 */

public class QuestionAnswer {
    private String mId;
    private String mText;
    private Integer mScore;
    private Boolean mIsCorrect;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public Integer getScore() {
        return mScore;
    }

    public void setScore(Integer score) {
        mScore = score;
    }

    public Boolean isCorrect() {
        return mIsCorrect;
    }

    public void setCorrect(Boolean correct) {
        mIsCorrect = correct;
    }
}
