package com.magycbytes.microsofttrainingkit.models;

/**
 * Created by sir Gregory on 2/9/2017.
 */

public class TestInformation {
    private String mExamName;
    private String mExamCode;

    public String getExamName() {
        return mExamName;
    }

    public void setExamName(String examName) {
        mExamName = examName;
    }

    public String getExamCode() {
        return mExamCode;
    }

    public void setExamCode(String examCode) {
        mExamCode = examCode;
    }
}
