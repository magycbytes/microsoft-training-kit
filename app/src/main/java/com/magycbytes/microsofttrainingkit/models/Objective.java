package com.magycbytes.microsofttrainingkit.models;

/**
 * Created by sir Gregory on 2/10/2017.
 */

public class Objective {
    private String mId;
    private String mName;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}
