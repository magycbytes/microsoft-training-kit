package com.magycbytes.microsofttrainingkit.models;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by sir Gregpry on 2/19/2017.
 */

public class TrainingContent {
    private List<Objective> mObjectives;
    private List<SubObjective> mSubObjectives;
    private List<Question> mQuestions;

    public TrainingContent() {
    }

    public List<Objective> getObjectives() {
        return mObjectives;
    }

    public void setObjectives(List<Objective> objectives) {
        mObjectives = objectives;
    }

    public List<SubObjective> getSubObjectives() {
        return mSubObjectives;
    }

    public void setSubObjectives(List<SubObjective> subObjectives) {
        mSubObjectives = subObjectives;
    }

    public List<Question> getQuestions() {
        return mQuestions;
    }

    public void setQuestions(List<Question> questions) {
        mQuestions = questions;
    }

    public String serilizeToJson() {
        return new Gson().toJson(this);
    }
}
