package com.magycbytes.microsofttrainingkit.models;

/**
 * Created by sir Gregory on 2/9/2017.
 */

public class ExamStatistics {
    private int mReferenceProducts;
    private int mReferences;
    private int mObjectives;
    private int mSubObjectives;
    private int mQuestions;
    private int mAnswers;
    private int mQuestionsReferenceLinks;

    public int getReferenceProducts() {
        return mReferenceProducts;
    }

    public void setReferenceProducts(int referenceProducts) {
        mReferenceProducts = referenceProducts;
    }

    public int getReferences() {
        return mReferences;
    }

    public void setReferences(int references) {
        mReferences = references;
    }

    public int getObjectives() {
        return mObjectives;
    }

    public void setObjectives(int objectives) {
        mObjectives = objectives;
    }

    public int getSubObjectives() {
        return mSubObjectives;
    }

    public void setSubObjectives(int subObjectives) {
        mSubObjectives = subObjectives;
    }

    public int getQuestions() {
        return mQuestions;
    }

    public void setQuestions(int questions) {
        mQuestions = questions;
    }

    public int getAnswers() {
        return mAnswers;
    }

    public void setAnswers(int answers) {
        mAnswers = answers;
    }

    public int getQuestionsReferenceLinks() {
        return mQuestionsReferenceLinks;
    }

    public void setQuestionsReferenceLinks(int questionsReferenceLinks) {
        mQuestionsReferenceLinks = questionsReferenceLinks;
    }
}
