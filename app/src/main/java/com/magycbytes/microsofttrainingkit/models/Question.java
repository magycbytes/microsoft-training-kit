package com.magycbytes.microsofttrainingkit.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by sir Gregpry on 2/19/2017.
 */

public class Question {

    private String mId;
    private String mObjectiveId;
    private String mText;
    private String mExplanation;
    private List<QuestionAnswer> mAnswers;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getObjectiveId() {
        return mObjectiveId;
    }

    public void setObjectiveId(String objectiveId) {
        mObjectiveId = objectiveId;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getExplanation() {
        return mExplanation;
    }

    public void setExplanation(String explanation) {
        mExplanation = explanation;
    }

    public List<QuestionAnswer> getAnswers() {
        return mAnswers;
    }

    public void setAnswers(List<QuestionAnswer> answers) {
        mAnswers = answers;
    }

    public void randomizeAnswers() {
        Collections.shuffle(mAnswers);
    }

    public boolean isTheAnswerCorrect(String answerId) {
        for (QuestionAnswer answer : mAnswers) {
            answer.getId().equals(answerId);
        }
        return false;
    }

    public boolean isMultipleAnswers() {
        int correctAnswers = 0;
        for (QuestionAnswer answer : mAnswers) {
            if (answer.isCorrect()) correctAnswers++;
        }

        return correctAnswers > 1;
    }

    public List<String> getCorrectIndexes() {
        List<String> correctIds = new ArrayList<>();

        for (int i = 0; i < mAnswers.size(); ++i) {
            if (mAnswers.get(i).isCorrect()) {
                correctIds.add(mAnswers.get(i).getId());
            }
        }

        return correctIds;
    }
}
