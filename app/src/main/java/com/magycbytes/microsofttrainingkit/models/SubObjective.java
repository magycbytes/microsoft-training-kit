package com.magycbytes.microsofttrainingkit.models;

/**
 * Created by sir Gregpry on 2/19/2017.
 */

public class SubObjective {

    private String mId;
    private String mName;
    private String mObjectiveId;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getObjectiveId() {
        return mObjectiveId;
    }

    public void setObjectiveId(String objectiveId) {
        mObjectiveId = objectiveId;
    }
}
