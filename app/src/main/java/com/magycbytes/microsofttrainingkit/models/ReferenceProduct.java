package com.magycbytes.microsofttrainingkit.models;

/**
 * Created by sir Gregpry on 2/19/2017.
 */

public class ReferenceProduct {

    private String mId;
    private String mName;
    private String mLink;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }
}
