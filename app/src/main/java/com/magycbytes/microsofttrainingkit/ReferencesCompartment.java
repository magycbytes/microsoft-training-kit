package com.magycbytes.microsofttrainingkit;

import com.google.gson.Gson;
import com.magycbytes.microsofttrainingkit.models.ReferenceProduct;
import com.magycbytes.microsofttrainingkit.models.TrainingContent;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sir Gregpry on 2/19/2017.
 */

public class ReferencesCompartment extends CompartmentParser {

    private List<ReferenceProduct> mReferences = new ArrayList<>();

    public ReferencesCompartment(BufferedReader bufferedReader) {
        super(bufferedReader);
    }

    @Override
    public String getSerializedResponse() {
        return new Gson().toJson(mReferences);
    }

    @Override
    public void saveLineValue(String fileLine) {
        String[] values = getSplitValues();

        ReferenceProduct referenceProduct = new ReferenceProduct();
        referenceProduct.setId(values[0]);
        referenceProduct.setName(values[1]);
        referenceProduct.setId(values[3]);

        mReferences.add(referenceProduct);
    }

    @Override
    public void visit(TrainingContent trainingContent) {

    }
}
