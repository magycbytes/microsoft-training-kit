package com.magycbytes.microsofttrainingkit.ui.certificationMode.responseView;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.magycbytes.microsofttrainingkit.models.QuestionAnswer;

import java.util.List;

/**
 * Created by Alex on 22/03/2017.
 */

class CheckBoxResponseContainer extends ResponseContainer {

    private static final String LOG_TAG = CheckBoxResponseContainer.class.getSimpleName();

    CheckBoxResponseContainer(Context context) {
        super(context);
    }

    @Override
    public void removeUserSelection() {

    }

    @Override
    public void showResponses(List<QuestionAnswer> answers, LinearLayout rootLayout) {
        for (QuestionAnswer answer : answers) {
            CompoundButton checkBox = new CheckBox(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 30);
            checkBox.setLayoutParams(layoutParams);

            checkBox.setText(answer.getText());
            checkBox.setBackgroundResource(android.R.color.transparent);

            rootLayout.addView(checkBox);
            mAnswerOptions.put(answer.getId(), checkBox);
        }
    }
}
