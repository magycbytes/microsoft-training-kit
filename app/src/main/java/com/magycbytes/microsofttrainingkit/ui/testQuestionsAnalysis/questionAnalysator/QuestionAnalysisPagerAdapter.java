package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.questionAnalysator;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


class QuestionAnalysisPagerAdapter extends FragmentPagerAdapter {

    private int mStartQuestion;
    private String[] mTitles;

    QuestionAnalysisPagerAdapter(FragmentManager fm, int startQuestion) {
        super(fm);
        mStartQuestion = startQuestion;

        mTitles = new String[] {
                "Question",
                "Explanation"
        };
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)  {
            return QuestionFragment.newInstance(mStartQuestion);
        }
        return ExplanationFragment.newInstance();
    }

    @Override
    public int getCount() {
        return mTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }
}
