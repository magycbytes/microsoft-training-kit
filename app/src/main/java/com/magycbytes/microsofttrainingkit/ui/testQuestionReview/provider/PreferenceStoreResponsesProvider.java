package com.magycbytes.microsofttrainingkit.ui.testQuestionReview.provider;

import android.content.Context;

import com.magycbytes.microsofttrainingkit.ui.certificationMode.UserResponsesContainer;
import com.magycbytes.microsofttrainingkit.util.ExamDatabase;

/**
 * Created by sir Gregpry on 2/22/2017.
 */

public class PreferenceStoreResponsesProvider implements QuestionsResponsesProvider {

    private Events mListener;
    private Context mContext;

    public PreferenceStoreResponsesProvider(Context context) {
        mContext = context;
    }

    @Override
    public void setEventsListener(Events listener) {
        mListener = listener;
    }

    @Override
    public void getAnswers() {
        String serializedUserResponses = ExamDatabase.getUserResponses(mContext);
        UserResponsesContainer responsesContainer = UserResponsesContainer.deserializeFromJson(serializedUserResponses);

        mListener.onAnsweredQuestionsAvailable(responsesContainer.getAnsweredQuestions());
        mListener.onUnansweredQuestionsAvailable(responsesContainer.getUnAnsweredQuestions());
        mListener.onElapsedTimeAvailable(responsesContainer.getElapsedTime());
    }
}
