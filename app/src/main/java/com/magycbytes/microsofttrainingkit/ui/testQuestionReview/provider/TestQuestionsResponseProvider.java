package com.magycbytes.microsofttrainingkit.ui.testQuestionReview.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by Alex on 22/02/2017.
 */

public class TestQuestionsResponseProvider implements QuestionsResponsesProvider {

    private Events mListener;

    @Override
    public void setEventsListener(Events listener) {
        mListener = listener;
    }

    @Override
    public void getAnswers() {
        List<Integer> answered = new ArrayList<>();
        List<Integer> unanswered = new ArrayList<>();

        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            answered.add(random.nextInt(40));
        }

        for (int i = 0; i < 15; ++i) {
            unanswered.add(random.nextInt(40));
        }

        Collections.sort(answered);
        Collections.sort(unanswered);

        mListener.onAnsweredQuestionsAvailable(answered);
        mListener.onUnansweredQuestionsAvailable(unanswered);
    }
}
