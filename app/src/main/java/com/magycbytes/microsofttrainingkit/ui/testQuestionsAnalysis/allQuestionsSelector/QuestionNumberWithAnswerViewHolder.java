package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.allQuestionsSelector;

import android.view.View;
import android.widget.ImageView;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.ui.testQuestionReview.QuestionNumberViewHolder;

import java.util.HashMap;

/**
 * Created by sir Gregpry on 2/24/2017.
 */

class QuestionNumberWithAnswerViewHolder extends QuestionNumberViewHolder {

    private ImageView mStatusIndicator;

    QuestionNumberWithAnswerViewHolder(View itemView, Events listener) {
        super(itemView, listener);

        mStatusIndicator = (ImageView) itemView.findViewById(R.id.responseStatus);
    }

    void showStatus(String answerStatus) {
        HashMap<String, Integer> statusesImages = new HashMap<>();
        statusesImages.put("Correct", R.drawable.ic_correct_answer);
        statusesImages.put("Incorrect", R.drawable.ic_incorrect_answer);
        statusesImages.put("Partial", -1);
        statusesImages.put("Unknown", R.drawable.ic_unknown_answer);

        mStatusIndicator.setImageResource(statusesImages.get(answerStatus));
    }
}
