package com.magycbytes.microsofttrainingkit.ui.testQuestionReview;

import com.magycbytes.microsofttrainingkit.ui.certificationMode.TimeCounter;
import com.magycbytes.microsofttrainingkit.ui.testQuestionReview.provider.QuestionsResponsesProvider;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.magycbytes.microsofttrainingkit.util.GlobalConstants.REMAINING_TIME_FORMAT;

/**
 * Created by Alex on 22/02/2017.
 */

public class TestQuestionsReviewPresenter implements QuestionsResponsesProvider.Events, TimeCounter.Events {


    private QuestionsResponsesProvider mResponsesProvider;
    private TestQuestionReviewView mView;
    private TimeCounter mTimeCounter;

    public TestQuestionsReviewPresenter(QuestionsResponsesProvider responsesProvider, TestQuestionReviewView view, TimeCounter timeCounter) {
        mResponsesProvider = responsesProvider;
        mView = view;
        mTimeCounter = timeCounter;

        mResponsesProvider.setEventsListener(this);
        mTimeCounter.setListener(this);
    }

    public void showAnswers() {
        mResponsesProvider.getAnswers();
    }

    @Override
    public void onAnsweredQuestionsAvailable(List<Integer> questionsNumbers) {
        mView.showAnsweredQuestions(questionsNumbers);
    }

    @Override
    public void onUnansweredQuestionsAvailable(List<Integer> questionsNumbers) {
        mView.showUnAnsweredQuestions(questionsNumbers);
    }

    @Override
    public void onElapsedTimeAvailable(long elapsedSeconds) {
        mTimeCounter.setElapsedTimeSeconds(elapsedSeconds);
        mTimeCounter.startCounting();
    }

    @Override
    public void onNewRemainingTimeAvailable(long elapsedSeconds) {
        long minutes = TimeUnit.SECONDS.toMinutes(elapsedSeconds);
        long seconds = elapsedSeconds - TimeUnit.MINUTES.toSeconds(minutes);

        mView.showRemaningTime(String.format(Locale.getDefault(), REMAINING_TIME_FORMAT, minutes, seconds));
    }
}
