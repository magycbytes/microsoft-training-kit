package com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.google.gson.Gson;
import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.models.Question;
import com.magycbytes.microsofttrainingkit.models.TrainingContent;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by sir Gregpry on 2/20/2017.
 */

public class AsyncJsonQuestionLoader extends AsyncTaskLoader<List<Question>> {

    private int mQuestionsCount;
    private List<Question> mLoadedQuestions;

    public AsyncJsonQuestionLoader(Context context) {
        super(context);

        mQuestionsCount = getContext().getResources().getInteger(R.integer.number_questions_certification_mode);
    }

    @Override
    public List<Question> loadInBackground() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(getContext().getResources().openRawResource(R.raw.test)));
        TrainingContent trainingContent = new Gson().fromJson(reader, TrainingContent.class);
        List<Question> questions = trainingContent.getQuestions();

        List<Question> testQuestions = new ArrayList<>();
        List<Integer> indexes = new ArrayList<>();
        Random random = new Random();
        boolean indexSelected;
        for (int i = 0; i < mQuestionsCount; ++i) {
            int generatedIndex;
            do {
                generatedIndex = random.nextInt(questions.size());
                indexSelected = !indexes.contains(generatedIndex);
            } while (!indexSelected);
            indexes.add(generatedIndex);

            Question question = questions.get(generatedIndex);
            question.randomizeAnswers();
            testQuestions.add(question);
        }

        return testQuestions;
    }

    @Override
    public void deliverResult(List<Question> data) {
        mLoadedQuestions = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mLoadedQuestions != null) {
            super.deliverResult(mLoadedQuestions);
        } else {
            forceLoad();
        }
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
    }
}
