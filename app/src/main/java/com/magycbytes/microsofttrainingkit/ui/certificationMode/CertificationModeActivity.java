package com.magycbytes.microsofttrainingkit.ui.certificationMode;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider.CertificationModeQuestionProvider;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider.JsonQuestionProvider;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider.ResumePreviousTestSessionProvider;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.responseView.ResponsesView;
import com.magycbytes.microsofttrainingkit.ui.testQuestionReview.TestQuestionReviewActivity;
import com.magycbytes.microsofttrainingkit.ui.testResults.TestResultsActivity;
import com.magycbytes.microsofttrainingkit.util.ExamDatabase;

import java.util.concurrent.TimeUnit;

public class CertificationModeActivity extends AppCompatActivity implements CertificationModePresenter.Events {

    public static final String RETAKE_THE_TEST = "RetakeTheTest";
    private CertificationModePresenter mPresenter;

    public static void start(Context context) {
        context.startActivity(new Intent(context, CertificationModeActivity.class));
    }

    public static void startForRetaking(Context context) {
        Intent intent = new Intent(context, CertificationModeActivity.class);
        intent.putExtra(RETAKE_THE_TEST, true);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_certification_mode);

        CertificationModeView view = new CertificationModeView(findViewById(R.id.rootLayout));
        CertificationModeQuestionProvider questionProvider;

        if (ExamDatabase.isPreviousSessionAvailable(this)) {
            boolean startFromFirstQuestion = getIntent().hasExtra(RETAKE_THE_TEST);
            questionProvider = new ResumePreviousTestSessionProvider(this, getSupportLoaderManager(), startFromFirstQuestion);
        } else {
            questionProvider = new JsonQuestionProvider(getSupportLoaderManager(), this);
        }

        TimeCounter timeCounter = new TimeCounter();
        timeCounter.setTotalAvailableTimeSeconds(TimeUnit.MINUTES.toSeconds(getResources().getInteger(R.integer.minutes_certification_mode)));
        mPresenter = new CertificationModePresenter(questionProvider, view, this, timeCounter, new ResponsesView(findViewById(R.id.rootLayout)));
        mPresenter.loadQuestions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_certification_mode, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.showQuestionsListReview:
                mPresenter.showResponseListForReview();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onShowTestResults() {
        TestResultsActivity.start(this);
        finish();
    }

    @Override
    public void onShowTestReview(String serializedResponses) {
        ExamDatabase.saveTestSession(this, serializedResponses);
        TestQuestionReviewActivity.start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TestQuestionReviewActivity.REQUEST_CODE && resultCode == RESULT_OK) {
            int questionNumber = data.getIntExtra(TestQuestionReviewActivity.QUESTION_NUMBER, 0);
            long elapsedTime = data.getLongExtra(TestQuestionReviewActivity.ELAPSED_TIME, 0);
            mPresenter.showQuestionNumber(questionNumber, elapsedTime);
        }
    }

    @Override
    protected void onStop() {
        ExamDatabase.saveTestSession(this, mPresenter.getUserResponsesSerialized());
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mPresenter.resumeTimer();
    }
}
