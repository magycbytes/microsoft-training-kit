package com.magycbytes.microsofttrainingkit.ui.certificationMode;

import android.text.Html;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.models.Question;


/**
 * Created by sir Gregpry on 2/20/2017.
 */

public class CertificationModeView implements ICertificationModeView, View.OnClickListener {

    private final View mPreviousQuestionButton;
    protected TextView mRemainingTime;
    private TextView mQuestionText;
    private TextView mCurrentQuestionNumber;
    private TextView mTotalQuestionsNumber;
    private Events mListener;
    private ScrollView mScrollContent;

    protected CertificationModeView(View view) {

        mQuestionText = (TextView) view.findViewById(R.id.questionContent);
        mCurrentQuestionNumber = (TextView) view.findViewById(R.id.questionNumber);
        mTotalQuestionsNumber = (TextView) view.findViewById(R.id.totalQuestions);
        mScrollContent = (ScrollView) view.findViewById(R.id.scrollContent);
        mPreviousQuestionButton = view.findViewById(R.id.previousQuestionButton);
        mRemainingTime = (TextView) view.findViewById(R.id.remainingTime);

        view.findViewById(R.id.nextQuestionButton).setOnClickListener(this);
        mPreviousQuestionButton.setOnClickListener(this);
    }

    @Override
    public void showLoadingProgress() {

    }

    @Override
    public void hideLoadingProgress() {

    }

    @Override
    public void showQuestion(Question question,String questionNumber, boolean isNotFirstQuestion) {
        mQuestionText.setText(Html.fromHtml(question.getText()));
        mCurrentQuestionNumber.setText(questionNumber);
        mScrollContent.smoothScrollTo(0, 0);

        mPreviousQuestionButton.setEnabled(isNotFirstQuestion);
    }

    @Override
    public void showTotalQuestionsNumber(String totalQuestionsNumber) {
        mTotalQuestionsNumber.setText(totalQuestionsNumber);
    }

    @Override
    public void showRemainingTime(String value) {
        mRemainingTime.setText(value);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nextQuestionButton:
                mListener.onShowNextQuestion();
                break;
            case R.id.previousQuestionButton:
                mListener.onShowPreviousQuestion();
                break;
        }
    }

    @Override
    public void setListener(Events listener) {
        mListener = listener;
    }
}
