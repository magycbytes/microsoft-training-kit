package com.magycbytes.microsofttrainingkit.ui.mainMenu;

import android.view.View;

import com.magycbytes.microsofttrainingkit.R;

/**
 * Created by sir Gregpry on 2/20/2017.
 */

public class MainView implements View.OnClickListener {

    private Events mListener;

    MainView(View view) {
        view.findViewById(R.id.certificationMode).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.certificationMode:
                mListener.onStartCertificationMode();
                break;
        }
    }

    public void setListener(Events listener) {
        mListener = listener;
    }

    interface Events {
        void onStartCertificationMode();
    }
}
