package com.magycbytes.microsofttrainingkit.ui.certificationMode.responseView;

import com.magycbytes.microsofttrainingkit.models.QuestionAnswer;

import java.util.List;

/**
 * Created by Alex on 22/03/2017.
 */

public interface IResponsesView {

    void showResponsesWithSingleAnswers(List<QuestionAnswer> answers);

    void showResponsesWithMultipleAnswers(List<QuestionAnswer> answers);

    void removeAnswersOptions();

    void showUserResponse(List<String> responseIds);

    List<String> getSelectedAnswerId();

    void showCorrectAnswers(List<String> correctIds);
}
