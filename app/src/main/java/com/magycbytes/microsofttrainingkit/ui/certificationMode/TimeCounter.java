package com.magycbytes.microsofttrainingkit.ui.certificationMode;

import android.os.Handler;

import java.util.concurrent.TimeUnit;

/**
 * Created by Alex on 24/02/2017.
 */
public class TimeCounter {

    private static final String LOG_TAG = TimeCounter.class.getSimpleName();

    private static final int UPDATE_INTERVAL_MILLISECONDS = 1;

    private long mElapsedTimeSeconds = 0;
    private long mTotalAvailableTime;
    private Events mListener;

    private Handler mHandler;
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
//            Log.d(LOG_TAG, "Updating timer counter");
            mElapsedTimeSeconds++;
            mHandler.postDelayed(mRunnable, TimeUnit.SECONDS.toMillis(UPDATE_INTERVAL_MILLISECONDS));
            if (mListener != null) {
                mListener.onNewRemainingTimeAvailable(mTotalAvailableTime - mElapsedTimeSeconds);
            }
        }
    };

    public long getElapsedTimeSeconds() {
        return mElapsedTimeSeconds;
    }

    public void setElapsedTimeSeconds(long elapsedTimeSeconds) {
        mElapsedTimeSeconds = elapsedTimeSeconds;
    }

    public void startCounting() {
        stopCounting();
        mHandler = new Handler();
        mHandler.postDelayed(mRunnable, UPDATE_INTERVAL_MILLISECONDS);
    }

    public void stopCounting() {
        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    public void setListener(Events listener) {
        mListener = listener;
    }

    public void setTotalAvailableTimeSeconds(long totalAvailableTime) {
        mTotalAvailableTime = totalAvailableTime;
    }

    public interface Events {
        void onNewRemainingTimeAvailable(long elapsedSeconds);
    }
}
