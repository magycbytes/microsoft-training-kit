package com.magycbytes.microsofttrainingkit.ui.testQuestionReview;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.magycbytes.microsofttrainingkit.R;

import java.util.List;

/**
 * Created by Alex on 22/02/2017.
 */

public class TestQuestionReviewView implements View.OnClickListener {

    private RecyclerView mUnAnsweredQuestionsContainer;
    private RecyclerView mAnsweredQuestionsContainer;
    private Events mListener;
    private QuestionNumberViewHolder.Events mQuestionNumbersListener;
    private TextView mRemainingTime;

    TestQuestionReviewView(View view, QuestionNumberViewHolder.Events questionNumbersListener) {
        mUnAnsweredQuestionsContainer = (RecyclerView) view.findViewById(R.id.unansweredQuestionsContainer);
        mAnsweredQuestionsContainer = (RecyclerView) view.findViewById(R.id.answeredQuestionsContainer);
        mRemainingTime = (TextView) view.findViewById(R.id.remainingTime);

        mQuestionNumbersListener = questionNumbersListener;
        int columnNumbers = view.getContext().getResources().getInteger(R.integer.column_numbers_questions_view);

        mUnAnsweredQuestionsContainer.setLayoutManager(new GridLayoutManager(view.getContext(), columnNumbers));
        mAnsweredQuestionsContainer.setLayoutManager(new GridLayoutManager(view.getContext(), columnNumbers));

        view.findViewById(R.id.scoreTestButton).setOnClickListener(this);
    }

    void showUnAnsweredQuestions(List<Integer> questionNumbers) {
        mUnAnsweredQuestionsContainer.setAdapter(new QuestionNumbersAdapter(questionNumbers, mQuestionNumbersListener));
    }

    void showAnsweredQuestions(List<Integer> questionNumbers) {
        mAnsweredQuestionsContainer.setAdapter(new QuestionNumbersAdapter(questionNumbers, mQuestionNumbersListener));
    }

    void showRemaningTime(String time) {
        mRemainingTime.setText(time);
    }

    @Override
    public void onClick(View v) {
        mListener.onScoreTheTest();
    }

    public void setQuestionNumbersListener(Events listener) {
        mListener = listener;
    }

    interface Events {
        void onScoreTheTest();
    }
}