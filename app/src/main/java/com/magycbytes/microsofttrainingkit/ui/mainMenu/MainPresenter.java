package com.magycbytes.microsofttrainingkit.ui.mainMenu;

/**
 * Created by sir Gregpry on 2/20/2017.
 */

public class MainPresenter implements MainView.Events {

    private MainView mMainView;

    public MainPresenter(MainView mainView) {
        mMainView = mainView;
        mMainView.setListener(this);
    }

    @Override
    public void onStartCertificationMode() {

    }
}
