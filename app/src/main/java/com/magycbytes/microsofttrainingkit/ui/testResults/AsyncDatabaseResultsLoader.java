package com.magycbytes.microsofttrainingkit.ui.testResults;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.UserResponsesContainer;
import com.magycbytes.microsofttrainingkit.util.ExamDatabase;
import com.magycbytes.microsofttrainingkit.util.GlobalConstants;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alex on 24/02/2017.
 */

public class AsyncDatabaseResultsLoader extends AsyncTaskLoader<TestResultsViewModel> {

    private TestResultsViewModel mOldData;

    public AsyncDatabaseResultsLoader(Context context) {
        super(context);
    }

    @Override
    public TestResultsViewModel loadInBackground() {
        UserResponsesContainer responsesContainer = UserResponsesContainer.deserializeFromJson(ExamDatabase.getUserResponses(getContext()));

        TestResultsViewModel viewModel = new TestResultsViewModel();
        viewModel.setCorrectAnsweredQuestions(responsesContainer.getCorrectRespondedQuestionsNumber());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        viewModel.setDateTaking(simpleDateFormat.format(new Date(responsesContainer.getDateTakingTheTest())));

        int totalExamQuestions = getContext().getResources().getInteger(R.integer.number_questions_certification_mode);
        int passingMinimumPercents = getContext().getResources().getInteger(R.integer.passing_percents);

        viewModel.setPassingPercents(getContext().getString(R.string.percents_value, passingMinimumPercents));
        viewModel.setPassingQuestionsNumber(getContext().getResources().getInteger(R.integer.passing_questions_number));
        viewModel.setTotalQuestions(totalExamQuestions);

        long minutes = TimeUnit.SECONDS.toMinutes(responsesContainer.getElapsedTime());
        long seconds = TimeUnit.SECONDS.toSeconds(responsesContainer.getElapsedTime() - TimeUnit.MINUTES.toSeconds(minutes));
        viewModel.setTookTime(String.format(Locale.getDefault(), GlobalConstants.REMAINING_TIME_FORMAT, minutes, seconds));

        float userTestPercentsFloat = (float) 100 * responsesContainer.getCorrectRespondedQuestionsNumber() / totalExamQuestions;
        int userTestPercents = (int) userTestPercentsFloat;

        viewModel.setScorePercents(getContext().getResources().getString(R.string.percents_value, userTestPercents));

        String statusPassingTest;
        if (userTestPercents < passingMinimumPercents) {
            statusPassingTest = "FAIL";
        } else {
            statusPassingTest = "Passed";
        }
        viewModel.setPassingStatus(statusPassingTest);


        return viewModel;
    }

    @Override
    public void deliverResult(TestResultsViewModel data) {
        mOldData = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mOldData != null) {
            super.deliverResult(mOldData);
        } else {
            forceLoad();
        }
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
    }
}
