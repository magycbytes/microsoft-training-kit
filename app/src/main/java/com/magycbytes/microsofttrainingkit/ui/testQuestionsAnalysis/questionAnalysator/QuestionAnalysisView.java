package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.questionAnalysator;

import android.view.View;

import com.magycbytes.microsofttrainingkit.models.Question;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.CertificationModeView;

/**
 * Created by Alex on 01/03/2017.
 */

public class QuestionAnalysisView extends CertificationModeView {

    QuestionAnalysisView(View view) {
        super(view);

        mRemainingTime.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showQuestion(Question question, String questionNumber, boolean isNotFirstQuestion) {
        super.showQuestion(question, questionNumber, isNotFirstQuestion);
    }
}
