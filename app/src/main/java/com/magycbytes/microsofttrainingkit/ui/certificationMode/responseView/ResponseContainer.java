package com.magycbytes.microsofttrainingkit.ui.certificationMode.responseView;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.models.QuestionAnswer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Alex on 22/03/2017.
 */

abstract class ResponseContainer {

    private static final String LOG_TAG = ResponseContainer.class.getSimpleName();

    Context mContext;
    HashMap<String, CompoundButton> mAnswerOptions = new HashMap<>();

    ResponseContainer(Context context) {
        mContext = context;
    }

    public abstract void removeUserSelection();

    public abstract void showResponses(List<QuestionAnswer> answers, LinearLayout rootLayout);

    public void showUserResponseWithoutColoring(List<String> responsesIds) {
        for (String responseId : responsesIds) {
            mAnswerOptions.get(responseId).setChecked(true);
        }
    }

    public void showCorrectUserResponses(List<String> responsesIds) {
        showUserResponseWithoutColoring(responsesIds);
    }

    public void showIncorrectUserResponses(List<String> responsesIds) {
        for (String responseId : responsesIds) {
            mAnswerOptions.get(responseId).setChecked(true);
            int incorrectAnswerColor = ContextCompat.getColor(mContext, R.color.incorrectAnswerBackground);
            mAnswerOptions.get(responseId).setBackgroundColor(incorrectAnswerColor);
        }
    }

    List<String> getSelectedAnswerId() {
        List<String> selectedAnswerIds = new ArrayList<>();

        for (String key : mAnswerOptions.keySet()) {
            if (mAnswerOptions.get(key).isChecked()) {
                selectedAnswerIds.add(key);
            }
        }

        return selectedAnswerIds;
    }

    void showCorrectAnswers(List<String> correctIds) {
        int backGroundColor = ContextCompat.getColor(mContext, R.color.correctAnswerBackground);
        int textColor = ContextCompat.getColor(mContext, android.R.color.white);

        Log.d(LOG_TAG, "Correct answers ids: " + Arrays.toString(correctIds.toArray()));

        for (String correctId : correctIds) {
            CompoundButton compoundButton = mAnswerOptions.get(correctId);
            compoundButton.setBackgroundColor(backGroundColor);
            compoundButton.setTextColor(textColor);
        }
    }

    void clean() {
        mAnswerOptions.clear();
    }
}
