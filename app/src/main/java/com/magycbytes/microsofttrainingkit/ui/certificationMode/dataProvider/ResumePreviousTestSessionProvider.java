package com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;

import com.magycbytes.microsofttrainingkit.models.Question;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.UserResponse;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.UserResponsesContainer;
import com.magycbytes.microsofttrainingkit.util.ExamDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 24/02/2017.
 */

public class ResumePreviousTestSessionProvider implements CertificationModeQuestionProvider, LoaderManager.LoaderCallbacks<List<Question>> {

    private static final String LOG_TAG = ResumePreviousTestSessionProvider.class.getSimpleName();

    private Events mListener;
    private Context mContext;
    private LoaderManager mLoaderManager;
    private List<String> mQuestionsIds;
    private UserResponsesContainer mResponsesContainer;
    private boolean mResetResponses;
    private int mQuestionIndexToStart = -1;

    public ResumePreviousTestSessionProvider(Context context, LoaderManager loaderManager, boolean resetResponses) {
        mContext = context;
        mLoaderManager = loaderManager;
        mResetResponses = resetResponses;

        Log.d(LOG_TAG, "Creating resume previous test question provider");
    }

    public void setQuestionNumberIndex(int questionNumberIndex) {
        mQuestionIndexToStart = questionNumberIndex;
    }

    @Override
    public void setListener(Events listener) {
        mListener = listener;
    }

    @Override
    public void getTestQuestions() {
        mResponsesContainer = UserResponsesContainer.deserializeFromJson(ExamDatabase.getUserResponses(mContext));
        mQuestionsIds = mResponsesContainer.getQuestionsIds();

        mLoaderManager.restartLoader(2, null, this);
    }

    @Override
    public Loader<List<Question>> onCreateLoader(int id, Bundle args) {
        return new AsyncJsonLastSessionQuestionLoader(mContext, mQuestionsIds);
    }

    @Override
    public void onLoadFinished(Loader<List<Question>> loader, List<Question> data) {
        int startQuestionIndex;
        if (mQuestionIndexToStart != -1) {
            startQuestionIndex = mQuestionIndexToStart;
        } else {
            startQuestionIndex = mResetResponses ? 0 : mResponsesContainer.getCurrentQuestionIndex();
        }

        long elapsedTime = mResponsesContainer.getElapsedTime();

        List<UserResponse> userResponses;
        if (mResetResponses) {
            userResponses = new ArrayList<>();
            // TODO move creating empty responses to async task
            for (Question question : data) {
                UserResponse userResponse = new UserResponse();
                userResponse.setQuestionId(question.getId());
                userResponses.add(userResponse);
            }
            elapsedTime = 0;
        } else {
            userResponses = mResponsesContainer.getUserResponses();
        }


        mListener.onTestQuestionsAvailable(data, userResponses, elapsedTime, startQuestionIndex);
    }

    @Override
    public void onLoaderReset(Loader<List<Question>> loader) {

    }
}
