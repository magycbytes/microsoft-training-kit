package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.allQuestionsSelector;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.UserResponsesContainer;
import com.magycbytes.microsofttrainingkit.ui.testQuestionReview.QuestionNumberViewHolder;
import com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.questionAnalysator.QuestionWithExplanationActivity;
import com.magycbytes.microsofttrainingkit.util.ExamDatabase;

public class AllQuestionsNumbersActivity extends AppCompatActivity implements QuestionNumberViewHolder.Events {

    public static void start(Context context) {
        context.startActivity(new Intent(context, AllQuestionsNumbersActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_questions_numbers);

        AllQuestionsNumbersView view = new AllQuestionsNumbersView(findViewById(R.id.rootLayout), this);
        // TODO implement through provider
        UserResponsesContainer responsesContainer = UserResponsesContainer.deserializeFromJson(ExamDatabase.getUserResponses(this));
        view.showUserResponses(responsesContainer.getUserResponses());
    }

    @Override
    public void onQuestionSelected(int questionNumber) {
        QuestionWithExplanationActivity.start(this, questionNumber - 1);
    }
}
