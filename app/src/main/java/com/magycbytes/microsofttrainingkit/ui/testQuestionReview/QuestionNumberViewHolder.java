package com.magycbytes.microsofttrainingkit.ui.testQuestionReview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.magycbytes.microsofttrainingkit.R;

/**
 * Created by Alex on 22/02/2017.
 */

public class QuestionNumberViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView mQuestionNumber;
    private Events mListener;

    public QuestionNumberViewHolder(View itemView, Events listener) {
        super(itemView);

        mListener = listener;

        mQuestionNumber = (TextView) itemView.findViewById(R.id.questionNumber);
        itemView.findViewById(R.id.rootLayout).setOnClickListener(this);
    }

    public void show(Integer questionNumber) {
        mQuestionNumber.setText(String.valueOf(questionNumber));
    }

    @Override
    public void onClick(View v) {
        mListener.onQuestionSelected(Integer.parseInt(mQuestionNumber.getText().toString()));
    }

    public interface Events {
        void onQuestionSelected(int questionNumber);
    }
}
