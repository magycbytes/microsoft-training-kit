package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.questionAnalysator;

import android.util.Log;

import com.magycbytes.microsofttrainingkit.models.Question;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.CertificationModePresenter;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.ICertificationModeView;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.TimeCounter;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider.CertificationModeQuestionProvider;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.responseView.IResponsesView;

/**
 * Created by sir Gregpry on 4/2/2017.
 */

public class QuestionAnalysatorPresenter extends CertificationModePresenter {

    private static final String LOG_TAG =  QuestionAnalysatorPresenter.class.getSimpleName();

    private QuestionAndExplanation mListener;

    public QuestionAnalysatorPresenter(CertificationModeQuestionProvider questionProvider, ICertificationModeView view, Events listener, TimeCounter timeCounter, IResponsesView responsesView) {
        super(questionProvider, view, listener, timeCounter, responsesView);
    }

    @Override
    protected void showCurrentQuestion() {
        super.showCurrentQuestion();

        if (mListener != null) {
            Question questionToShow = mQuestions.get(mCurrentQuestionIndex);
            mListener.onShowExplanation(questionToShow.getExplanation());
        } else {
            Log.d(LOG_TAG, "Listener is null");
        }
    }

    public void setListener(QuestionAndExplanation listener) {
        mListener = listener;
    }
}
