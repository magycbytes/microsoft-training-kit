package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.questionAnalysator;

import android.widget.LinearLayout;

import com.magycbytes.microsofttrainingkit.customViews.QuestionWithResponse;
import com.magycbytes.microsofttrainingkit.models.QuestionAnswer;

import java.util.List;

/**
 * Created by Alex on 15/03/2017.
 */

public class ResponseWithAnswersView {

    private LinearLayout mContainer;

    public ResponseWithAnswersView(LinearLayout container) {
        mContainer = container;
    }

    public void showResponses(List<QuestionAnswer> answers) {
        mContainer.removeAllViews();

        for (QuestionAnswer questionAnswer : answers) {
            QuestionWithResponse questionWithResponse = new QuestionWithResponse(mContainer.getContext());
            questionWithResponse.show(questionAnswer.getText(), "");
        }
    }
}
