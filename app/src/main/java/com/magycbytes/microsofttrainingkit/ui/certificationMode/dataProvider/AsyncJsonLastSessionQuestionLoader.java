package com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.google.gson.Gson;
import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.models.Question;
import com.magycbytes.microsofttrainingkit.models.TrainingContent;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sir Gregpry on 2/20/2017.
 */

public class AsyncJsonLastSessionQuestionLoader extends AsyncTaskLoader<List<Question>> {


    private List<Question> mLoadedQuestions;
    private List<String> mLastTestSessionsIds;

    public AsyncJsonLastSessionQuestionLoader(Context context, List<String> lastTestSessionsIds) {
        super(context);
        mLastTestSessionsIds = lastTestSessionsIds;
    }

    @Override
    public List<Question> loadInBackground() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(getContext().getResources().openRawResource(R.raw.test)));
        TrainingContent trainingContent = new Gson().fromJson(reader, TrainingContent.class);
        List<Question> questions = trainingContent.getQuestions();

        List<Question> testQuestions = new ArrayList<>();
        for (String questionId : mLastTestSessionsIds) {
            for (Question question : questions) {
                if (question.getId().equals(questionId)) {
                    question.randomizeAnswers();
                    testQuestions.add(question);
                    break;
                }
            }
        }

        return testQuestions;
    }

    @Override
    public void deliverResult(List<Question> data) {
        mLoadedQuestions = data;

        if (isStarted()) super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if (mLoadedQuestions != null) {
            super.deliverResult(mLoadedQuestions);
        } else {
            forceLoad();
        }
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
    }
}
