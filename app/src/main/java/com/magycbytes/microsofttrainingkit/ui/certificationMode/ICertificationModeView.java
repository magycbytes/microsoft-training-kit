package com.magycbytes.microsofttrainingkit.ui.certificationMode;

import com.magycbytes.microsofttrainingkit.models.Question;

/**
 * Created by Alex on 22/03/2017.
 */

public interface ICertificationModeView extends IProgressView {

    void showQuestion(Question question, String questionNumber, boolean isNotFirstQuestion);

    void showTotalQuestionsNumber(String totalQuestionsNumber);

    void showRemainingTime(String value);

    void setListener(Events listener);

    interface Events {
        void onShowNextQuestion();

        void onShowPreviousQuestion();
    }
}
