package com.magycbytes.microsofttrainingkit.ui.certificationMode;

/**
 * Created by Alex on 22/03/2017.
 */

public interface IProgressView {

    void showLoadingProgress();

    void hideLoadingProgress();
}
