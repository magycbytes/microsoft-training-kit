package com.magycbytes.microsofttrainingkit.ui.certificationMode.responseView;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.magycbytes.microsofttrainingkit.models.QuestionAnswer;

import java.util.List;

/**
 * Created by Alex on 22/03/2017.
 */

class RadioButtonResponseContainer extends ResponseContainer {

    private RadioGroup mAnswersContainer;

    RadioButtonResponseContainer(Context context) {
        super(context);

        mAnswersContainer = new RadioGroup(context);
    }

    @Override
    public void removeUserSelection() {
        mAnswersContainer.clearCheck();
    }

    @Override
    public void showResponses(List<QuestionAnswer> answers, LinearLayout rootLayout) {
        for (int i = 0; i < answers.size(); ++i) {
            CompoundButton radioButton = new RadioButton(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 30);
            radioButton.setLayoutParams(layoutParams);

            radioButton.setText(answers.get(i).getText());
            radioButton.setBackgroundResource(android.R.color.transparent);

            mAnswersContainer.addView(radioButton);
            mAnswerOptions.put(answers.get(i).getId(), radioButton);
        }

        rootLayout.addView(mAnswersContainer);
    }
}
