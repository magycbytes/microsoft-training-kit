package com.magycbytes.microsofttrainingkit.ui.testQuestionReview;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.TimeCounter;
import com.magycbytes.microsofttrainingkit.ui.testQuestionReview.provider.PreferenceStoreResponsesProvider;
import com.magycbytes.microsofttrainingkit.ui.testQuestionReview.provider.QuestionsResponsesProvider;
import com.magycbytes.microsofttrainingkit.ui.testResults.TestResultsActivity;

import java.util.concurrent.TimeUnit;

public class TestQuestionReviewActivity extends AppCompatActivity implements TestQuestionReviewView.Events, QuestionNumberViewHolder.Events {

    public static final String QUESTION_NUMBER = "QuestionNumber";
    public static final String ELAPSED_TIME = "ElapsedTime";
    public static final int REQUEST_CODE = 20;
    private TimeCounter mTimeCounter;

    public static void start(Activity context) {
        context.startActivityForResult(new Intent(context, TestQuestionReviewActivity.class), REQUEST_CODE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_question_review);

        TestQuestionReviewView view = new TestQuestionReviewView(findViewById(R.id.rootLayout), this);
        view.setQuestionNumbersListener(this);

        QuestionsResponsesProvider provider = new PreferenceStoreResponsesProvider(this);
        mTimeCounter = new TimeCounter();
        mTimeCounter.setTotalAvailableTimeSeconds(TimeUnit.MINUTES.toSeconds(getResources().getInteger(R.integer.minutes_certification_mode)));

        TestQuestionsReviewPresenter presenter = new TestQuestionsReviewPresenter(provider, view, mTimeCounter);
        presenter.showAnswers();
    }

    @Override
    public void onScoreTheTest() {
        TestResultsActivity.start(this);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onQuestionSelected(int questionNumber) {
        Intent data = new Intent();
        data.putExtra(QUESTION_NUMBER, questionNumber - 1);
        data.putExtra(ELAPSED_TIME, mTimeCounter.getElapsedTimeSeconds());
        setResult(RESULT_OK, data);

        finish();
    }

    @Override
    protected void onStop() {
        mTimeCounter.stopCounting();
        super.onStop();
    }
}
