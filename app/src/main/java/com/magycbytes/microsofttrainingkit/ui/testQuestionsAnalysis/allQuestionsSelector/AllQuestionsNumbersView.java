package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.allQuestionsSelector;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.UserResponse;
import com.magycbytes.microsofttrainingkit.ui.testQuestionReview.QuestionNumberViewHolder;

import java.util.List;

/**
 * Created by sir Gregpry on 2/26/2017.
 */

class AllQuestionsNumbersView {

    private RecyclerView mQuestionsContainer;
    private QuestionNumberViewHolder.Events mListener;

    AllQuestionsNumbersView(View view, QuestionNumberViewHolder.Events listener) {
        mQuestionsContainer = (RecyclerView) view.findViewById(R.id.questionNumbersContainer);
        mListener = listener;

        int columnsNumber = view.getContext().getResources().getInteger(R.integer.column_numbers_questions_analysis);
        mQuestionsContainer.setLayoutManager(new GridLayoutManager(view.getContext(), columnsNumber));
    }

    void showUserResponses(List<UserResponse> userResponses) {
        mQuestionsContainer.setAdapter(new QuestionNumbersAdapter(userResponses, mListener));
    }
}
