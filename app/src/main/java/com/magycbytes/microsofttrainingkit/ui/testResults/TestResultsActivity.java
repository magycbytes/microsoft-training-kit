package com.magycbytes.microsofttrainingkit.ui.testResults;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.CertificationModeActivity;
import com.magycbytes.microsofttrainingkit.ui.mainMenu.MainMenuActivity;
import com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.allQuestionsSelector.AllQuestionsNumbersActivity;

public class TestResultsActivity extends AppCompatActivity implements TestResultsView.Events {

    public static void start(Context context) {
        Intent intent = new Intent(context, TestResultsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_results);

        TestResultsView view = new TestResultsView(findViewById(R.id.rootLayout));
        view.setListener(this);

        TestResultsPresenter presenter = new TestResultsPresenter(new DatabaseResultsProvider(this, getSupportLoaderManager()), view);
        presenter.showResults();
    }

    @Override
    public void onExitFromCurrentTest() {
        MainMenuActivity.start(this);
        finish();
    }

    @Override
    public void onRetakeTest() {
        CertificationModeActivity.startForRetaking(this);
        finish();
    }

    @Override
    public void onReviewQuestions() {
        AllQuestionsNumbersActivity.start(this);
    }

    @Override
    public void onBackPressed() {
        onExitFromCurrentTest();
    }
}
