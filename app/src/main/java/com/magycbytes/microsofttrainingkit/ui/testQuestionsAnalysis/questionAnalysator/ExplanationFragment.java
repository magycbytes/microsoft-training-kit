package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.questionAnalysator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.magycbytes.microsofttrainingkit.R;


public class ExplanationFragment extends Fragment {

    private static final String LOG_TAG = ExplanationFragment.class.getSimpleName();

    private TextView mExplanation;

    public static ExplanationFragment newInstance() {
        Log.d(LOG_TAG, "Creating new instance");
        return new ExplanationFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_explanation_question, container, false);

        mExplanation = (TextView) rootView.findViewById(R.id.explanation);
        return rootView;
    }

    public void showExplanation(String explanation) {
        Log.d(LOG_TAG, "Showing explanation");
        mExplanation.setText(Html.fromHtml(explanation));
    }
}
