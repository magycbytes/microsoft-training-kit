package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.questionAnalysator;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.CertificationModePresenter;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.TimeCounter;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider.CertificationModeQuestionProvider;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider.JsonQuestionProvider;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider.ResumePreviousTestSessionProvider;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.responseView.ResponsesView;
import com.magycbytes.microsofttrainingkit.util.ExamDatabase;

import java.util.concurrent.TimeUnit;

/**
 * Created by Alex on 16/03/2017.
 */

public class QuestionFragment extends Fragment implements CertificationModePresenter.Events {

    private static final String LOG_TAG = QuestionFragment.class.getSimpleName();

    public static final String QUESTION_INDEX = "QuestionIndex";
    private QuestionAnalysatorPresenter mPresenter;
    private QuestionAndExplanation mListener;

    public static QuestionFragment newInstance(int questionIndex) {
        Log.d(LOG_TAG, "Creating new instance");

        Bundle bundle = new Bundle();
        bundle.putInt(QUESTION_INDEX, questionIndex);

        QuestionFragment questionFragment = new QuestionFragment();
        questionFragment.setArguments(bundle);

        return questionFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_certification_mode, container, false);


        QuestionAnalysisView view = new QuestionAnalysisView(inflate.findViewById(R.id.rootLayout));
        CertificationModeQuestionProvider questionProvider;

        if (ExamDatabase.isPreviousSessionAvailable(getContext())) {
            ResumePreviousTestSessionProvider resumeProvider = new ResumePreviousTestSessionProvider(getContext(), getLoaderManager(), false);
            resumeProvider.setQuestionNumberIndex(0);
            resumeProvider.setQuestionNumberIndex(getArguments().getInt(QUESTION_INDEX, 0));

            questionProvider = resumeProvider;
        } else {
            questionProvider = new JsonQuestionProvider(getLoaderManager(), getContext());
        }

        TimeCounter timeCounter = new TimeCounter();
        timeCounter.setTotalAvailableTimeSeconds(TimeUnit.MINUTES.toSeconds(getResources().getInteger(R.integer.minutes_certification_mode)));
        mPresenter = new QuestionAnalysatorPresenter(questionProvider, view, this, timeCounter, new ResponsesView(inflate.findViewById(R.id.rootLayout)));
        mPresenter.setListener(mListener);
        mPresenter.loadQuestions();

        return inflate;
    }

    @Override
    public void onShowTestResults() {

    }

    @Override
    public void onShowTestReview(String serializedResponses) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mListener = (QuestionAndExplanation) context;
    }

    @Override
    public void onDetach() {
        if (mPresenter != null) {
            mPresenter.setListener(null);
        }
        mListener = null;
        super.onDetach();
    }
}
