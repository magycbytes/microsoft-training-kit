package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.questionAnalysator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.magycbytes.microsofttrainingkit.R;

import java.util.List;

public class QuestionWithExplanationActivity extends AppCompatActivity implements QuestionAndExplanation {

    public static final String QUESTION_INDEX = "QuestionIndex";

    private String mExplanation = "";

    public static void start(Context context, int questionIndex) {
        Intent intent = new Intent(context, QuestionWithExplanationActivity.class);
        intent.putExtra(QUESTION_INDEX, questionIndex);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_with_explanation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        QuestionAnalysisPagerAdapter questionAnalysisPagerAdapter = new QuestionAnalysisPagerAdapter(getSupportFragmentManager(), getIntent().getIntExtra(QUESTION_INDEX, 0));

        // Set up the ViewPager with the sections adapter.
        ViewPager viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(questionAnalysisPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onShowExplanation(String explanation) {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (int i = 0; i < fragments.size(); ++i) {
            if (fragments.get(i) instanceof ExplanationFragment) {
                ((ExplanationFragment) fragments.get(i)).showExplanation(explanation);
                return;
            }
        }

        mExplanation = explanation;
    }
}
