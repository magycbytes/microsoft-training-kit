package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.questionAnalysator;

import com.magycbytes.microsofttrainingkit.models.Question;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.CertificationModePresenter;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.ICertificationModeView;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.TimeCounter;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider.CertificationModeQuestionProvider;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.responseView.IResponsesView;

/**
 * Created by Alex on 23/03/2017.
 */

public class QuestionAnalysisPresenter extends CertificationModePresenter {

    public QuestionAnalysisPresenter(CertificationModeQuestionProvider questionProvider, ICertificationModeView view, Events listener, TimeCounter timeCounter, IResponsesView responsesView) {
        super(questionProvider, view, listener, timeCounter, responsesView);
    }

    @Override
    protected void showUserResponse(Question questionToShow) {

    }
}
