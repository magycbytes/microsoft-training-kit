package com.magycbytes.microsofttrainingkit.ui.certificationMode;

import android.util.Log;

import com.magycbytes.microsofttrainingkit.models.Question;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider.CertificationModeQuestionProvider;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.responseView.IResponsesView;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by sir Gregpry on 2/20/2017.
 */

public class CertificationModePresenter implements CertificationModeQuestionProvider.Events, CertificationModeView.Events, TimeCounter.Events {

    private static final String LOG_TAG = CertificationModePresenter.class.getSimpleName();

    private CertificationModeQuestionProvider mQuestionProvider;
    protected ICertificationModeView mView;
    protected List<Question> mQuestions;
    protected int mCurrentQuestionIndex = 0;
    private Events mListener;
    protected UserResponsesContainer mResponsesContainer = new UserResponsesContainer();
    private TimeCounter mTimeCounter;
    protected IResponsesView mResponsesView;

    public CertificationModePresenter(CertificationModeQuestionProvider questionProvider, ICertificationModeView view, Events listener, TimeCounter timeCounter, IResponsesView responsesView) {
        mQuestionProvider = questionProvider;
        mView = view;
        mListener = listener;
        mTimeCounter = timeCounter;
        mResponsesView = responsesView;

        mView.setListener(this);
        mQuestionProvider.setListener(this);
        mTimeCounter.setListener(this);
    }

    void resumeTimer() {
        mTimeCounter.startCounting();
    }

    public void loadQuestions() {
        Log.d(LOG_TAG, "Loading questions");
        mView.showLoadingProgress();
        mQuestionProvider.getTestQuestions();
    }

    void showResponseListForReview() {
        mListener.onShowTestReview(mResponsesContainer.serializeToJson(mTimeCounter.getElapsedTimeSeconds()));
    }

    void showQuestionNumber(int questionNumber, long elapsedTimeSeconds) {
        mCurrentQuestionIndex = questionNumber;
        mTimeCounter.setElapsedTimeSeconds(elapsedTimeSeconds);
        mTimeCounter.startCounting();
        showCurrentQuestion();
    }

    @Override
    public void onShowNextQuestion() {
        mResponsesContainer.addResponse(mQuestions.get(mCurrentQuestionIndex).getId(), mResponsesView.getSelectedAnswerId());

        if ((mCurrentQuestionIndex + 1) >= mQuestions.size()) {
            showResponseListForReview();
        } else {
            mCurrentQuestionIndex++;
            showCurrentQuestion();
        }
    }

    @Override
    public void onShowPreviousQuestion() {
        mCurrentQuestionIndex--;
        showCurrentQuestion();
    }

    String getUserResponsesSerialized() {
        mTimeCounter.stopCounting();
        return mResponsesContainer.serializeToJson(mTimeCounter.getElapsedTimeSeconds());
    }

    protected void showCurrentQuestion() {
        mResponsesContainer.setCurrentQuestionIndex(mCurrentQuestionIndex);

        Question questionToShow = mQuestions.get(mCurrentQuestionIndex);
        int questionNumber = mCurrentQuestionIndex + 1;
        mView.showQuestion(questionToShow, String.valueOf(questionNumber), questionNumber > 1);

        if (questionToShow.isMultipleAnswers()) {
            mResponsesView.showResponsesWithMultipleAnswers(questionToShow.getAnswers());
        } else {
            mResponsesView.showResponsesWithSingleAnswers(questionToShow.getAnswers());
        }
        showUserResponse(questionToShow);
    }

    protected void showUserResponse(Question questionToShow) {
        List<String> userResponseIndex = mResponsesContainer.getUserResponseIndex(questionToShow.getId());
        if (userResponseIndex.isEmpty()) {
            mResponsesView.removeAnswersOptions();
        } else {
            mResponsesView.showUserResponse(userResponseIndex);
        }
    }

    @Override
    public void onTestQuestionsAvailable(List<Question> questions, List<UserResponse> userResponses, long spentTime, int questionIndex) {
        Log.d(LOG_TAG, "Test questions available");
        mQuestions = questions;
        mCurrentQuestionIndex = questionIndex;
        mTimeCounter.setElapsedTimeSeconds(spentTime);
        mTimeCounter.startCounting();

        mResponsesContainer.setTotalQuestionsNumber(questions.size());
        mResponsesContainer.setUserResponses(userResponses);
        mResponsesContainer.setCurrentQuestionIndex(questionIndex);

        mView.hideLoadingProgress();
        mView.showTotalQuestionsNumber(String.valueOf(questions.size()));
        showCurrentQuestion();
    }

    @Override
    public void onNewRemainingTimeAvailable(long elapsedSeconds) {
        long minutes = TimeUnit.SECONDS.toMinutes(elapsedSeconds);
        long seconds = elapsedSeconds - TimeUnit.MINUTES.toSeconds(minutes);

        mView.showRemainingTime(String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds));
    }

    public interface Events {
        void onShowTestResults();

        void onShowTestReview(String serializedResponses);
    }
}
