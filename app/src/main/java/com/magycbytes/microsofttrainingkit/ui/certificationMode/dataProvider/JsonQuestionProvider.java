package com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;

import com.magycbytes.microsofttrainingkit.models.Question;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.UserResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sir Gregpry on 2/20/2017.
 */

public class JsonQuestionProvider implements CertificationModeQuestionProvider, LoaderManager.LoaderCallbacks<List<Question>> {

    private static final String LOG_TAG = JsonQuestionProvider.class.getSimpleName();

    private Events mListener;
    private LoaderManager mLoaderManager;
    private Context mContext;

    public JsonQuestionProvider(LoaderManager loaderManager, Context context) {
        mLoaderManager = loaderManager;
        mContext = context;

        Log.d(LOG_TAG, "Creating new json random test question provider");
    }

    @Override
    public void setListener(Events listener) {
        mListener = listener;
    }

    @Override
    public void getTestQuestions() {
        mLoaderManager.initLoader(1, null, this);
    }

    @Override
    public Loader<List<Question>> onCreateLoader(int id, Bundle args) {
        return new AsyncJsonQuestionLoader(mContext);
    }

    @Override
    public void onLoadFinished(Loader<List<Question>> loader, List<Question> data) {
        List<UserResponse> userResponses = new ArrayList<>();

        // TODO move creating empty responses to async task
        for (Question question : data) {
            UserResponse userResponse = new UserResponse();
            userResponse.setQuestionId(question.getId());
            userResponses.add(userResponse);
        }
        mListener.onTestQuestionsAvailable(data, userResponses, 0, 0);
    }

    @Override
    public void onLoaderReset(Loader<List<Question>> loader) {

    }
}
