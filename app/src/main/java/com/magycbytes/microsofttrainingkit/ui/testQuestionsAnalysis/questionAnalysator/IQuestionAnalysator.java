package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.questionAnalysator;

import com.magycbytes.microsofttrainingkit.ui.certificationMode.ICertificationModeView;

/**
 * Created by sir Gregpry on 4/5/2017.
 */

public interface IQuestionAnalysator extends ICertificationModeView {

    void showUserResponse();

}
