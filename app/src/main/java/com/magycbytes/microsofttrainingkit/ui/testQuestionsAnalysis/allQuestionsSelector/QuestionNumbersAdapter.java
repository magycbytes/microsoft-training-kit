package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.allQuestionsSelector;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.UserResponse;
import com.magycbytes.microsofttrainingkit.ui.testQuestionReview.QuestionNumberViewHolder;

import java.util.List;

/**
 * Created by sir Gregpry on 2/24/2017.
 */

class QuestionNumbersAdapter extends RecyclerView.Adapter<QuestionNumberWithAnswerViewHolder> {

    private List<UserResponse> mUserResponses;
    private QuestionNumberViewHolder.Events mListener;

    QuestionNumbersAdapter(List<UserResponse> userResponses, QuestionNumberViewHolder.Events listener) {
        mUserResponses = userResponses;
        mListener = listener;
    }

    @Override
    public QuestionNumberWithAnswerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_question_number_response_status, parent, false);
        return new QuestionNumberWithAnswerViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(QuestionNumberWithAnswerViewHolder holder, int position) {
        holder.show(position + 1);

        holder.showStatus(mUserResponses.get(position).getStatus());
    }

    @Override
    public int getItemCount() {
        return mUserResponses.size();
    }
}
