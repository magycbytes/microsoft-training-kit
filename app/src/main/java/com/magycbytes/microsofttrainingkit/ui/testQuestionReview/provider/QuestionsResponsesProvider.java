package com.magycbytes.microsofttrainingkit.ui.testQuestionReview.provider;

import java.util.List;

/**
 * Created by Alex on 22/02/2017.
 */

public interface QuestionsResponsesProvider {

    void setEventsListener(Events listener);

    void getAnswers();

    interface Events {
        void onAnsweredQuestionsAvailable(List<Integer> questionsNumbers);

        void onUnansweredQuestionsAvailable(List<Integer> questionsNumbers);

        void onElapsedTimeAvailable(long elapsedSeconds);
    }
}
