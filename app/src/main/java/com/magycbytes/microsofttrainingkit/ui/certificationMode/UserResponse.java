package com.magycbytes.microsofttrainingkit.ui.certificationMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sir Gregpry on 2/22/2017.
 */

public class UserResponse {

    private String mQuestionId;
    private List<String> mAnswerIds = new ArrayList<>();
    private List<String> mCorrectAnswerIds = new ArrayList<>();

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String questionId) {
        mQuestionId = questionId;
    }

    public String getStatus() {
        if (mAnswerIds.isEmpty()) {
            return "Unknown";
        } else {
            int userCorrectAnswers = 0;
            for (String userAnswerId : mAnswerIds) {
                if (mCorrectAnswerIds.contains(userAnswerId)) {
                    userCorrectAnswers++;
                }
            }
            if (userCorrectAnswers == mCorrectAnswerIds.size()) {
                return "Correct";
            } else if (userCorrectAnswers > 1) {
                return "Partial";
            } else {
                return "Incorrect";
            }
        }
    }

    public boolean isCorrect() {
        return getStatus().equals("Correct");
    }

    public List<String> getAnswerIds() {
        return mAnswerIds;
    }

    public void setAnswerIds(List<String> answerIds) {
        mAnswerIds = answerIds;
    }

    public boolean didUserProvidedAResponse() {
        return !mAnswerIds.isEmpty();
    }
}
