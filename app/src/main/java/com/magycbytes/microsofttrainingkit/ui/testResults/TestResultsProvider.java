package com.magycbytes.microsofttrainingkit.ui.testResults;

/**
 * Created by Alex on 24/02/2017.
 */

public interface TestResultsProvider {

    void getResults();

    void setListener(Events listener);

    interface Events {
        void onResultsAvailable(TestResultsViewModel viewModel);
    }
}
