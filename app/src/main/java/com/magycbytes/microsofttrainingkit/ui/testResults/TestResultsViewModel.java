package com.magycbytes.microsofttrainingkit.ui.testResults;

/**
 * Created by Alex on 24/02/2017.
 */

public class TestResultsViewModel {

    private String mDateTaking;
    private String mTookTime;
    private String mScorePercents;
    private String mCorrectAnsweredQuestions;
    private String mTotalQuestions;
    private String mPassingPercents;
    private String mPassingQuestionsNumber;
    private String mPassingStatus;

    public void setDateTaking(String dateTaking) {
        mDateTaking = dateTaking;
    }

    public void setTookTime(String tookTime) {
        mTookTime = tookTime;
    }

    public void setScorePercents(String scorePercents) {
        mScorePercents = scorePercents;
    }

    public void setCorrectAnsweredQuestions(int correctAnsweredQuestions) {
        mCorrectAnsweredQuestions = String.valueOf(correctAnsweredQuestions);
    }

    public void setTotalQuestions(int totalQuestions) {
        mTotalQuestions = String.valueOf(totalQuestions);
    }

    public void setPassingPercents(String passingPercents) {
        mPassingPercents = passingPercents;
    }

    public void setPassingQuestionsNumber(int passingQuestionsNumber) {
        mPassingQuestionsNumber = String.valueOf(passingQuestionsNumber);
    }

    public void setPassingStatus(String passingStatus) {
        mPassingStatus = passingStatus;
    }

    public String getDateTaking() {
        return mDateTaking;
    }

    public String getTookTime() {
        return mTookTime;
    }

    public String getScorePercents() {
        return mScorePercents;
    }

    public int getScorePercentsInt() {
        return Integer.parseInt(mScorePercents.substring(0, mScorePercents.indexOf("%")));
    }

    public String getCorrectAnsweredQuestions() {
        return mCorrectAnsweredQuestions + "/" + mTotalQuestions;
    }

    public String getTotalQuestions() {
        return mTotalQuestions;
    }

    public String getPassingPercents() {
        return mPassingPercents;
    }

    public String getPassingQuestionsNumber() {
        return mPassingQuestionsNumber + "/" + mTotalQuestions;
    }

    public String getPassingStatus() {
        return mPassingStatus;
    }
}
