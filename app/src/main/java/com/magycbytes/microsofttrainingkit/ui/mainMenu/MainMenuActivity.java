package com.magycbytes.microsofttrainingkit.ui.mainMenu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.CertificationModeActivity;
import com.magycbytes.microsofttrainingkit.util.ExamDatabase;

public class MainMenuActivity extends AppCompatActivity implements MainView.Events {

    public static void start(Context context) {
        context.startActivity(new Intent(context, MainMenuActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        MainView view = new MainView(findViewById(R.id.rootLayout));
        view.setListener(this);

        onStartCertificationMode();
    }

    @Override
    public void onStartCertificationMode() {
        ExamDatabase.resetLastSession(this);
        CertificationModeActivity.start(this);
    }
}
