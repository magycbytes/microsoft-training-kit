package com.magycbytes.microsofttrainingkit.ui.certificationMode;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by sir Gregpry on 2/22/2017.
 */

public class UserResponsesContainer {

    private List<UserResponse> mUserResponses = new ArrayList<>();
    private long dateTakingTheTest = 0;
    private int mTotalQuestionsNumber = 0;
    private int mCurrentQuestionIndex = 0;
    private long mElapsedTime;

    void addResponse(String questionId, List<String> answersIds) {
        for (int i = 0; i < mUserResponses.size(); ++i) {
            UserResponse availableResponse = mUserResponses.get(i);
            if (availableResponse.getQuestionId().equals(questionId)) {
                availableResponse.setAnswerIds(answersIds);
                return;
            }
        }
    }

    void setUserResponses(List<UserResponse> userResponses) {
        mUserResponses = userResponses;
    }

    String serializeToJson(long elapsedTime) {
        dateTakingTheTest = Calendar.getInstance().getTimeInMillis();
        mElapsedTime = elapsedTime;
        return new Gson().toJson(this);
    }

    boolean doesUserRespondedTheQuestion(String questionId) {
        for (UserResponse userResponse : mUserResponses) {
            if (userResponse.getQuestionId().equals(questionId)) {
                return userResponse.didUserProvidedAResponse();
            }
        }
        return false;
    }

    public List<String> getUserResponseIndex(String questionId) {
        for (UserResponse userResponse : mUserResponses) {
            if (userResponse.getQuestionId().equals(questionId)) {
                return userResponse.getAnswerIds();
            }
        }
        return new ArrayList<>();
    }

    public static UserResponsesContainer deserializeFromJson(String serializedData) {
        return new Gson().fromJson(serializedData, UserResponsesContainer.class);
    }

    void setTotalQuestionsNumber(int totalQuestionsNumber) {
        mTotalQuestionsNumber = totalQuestionsNumber;
    }

    public List<Integer> getAnsweredQuestions() {
        List<Integer> answeredQuestionsPositions = new ArrayList<>();

        for (int i = 0; i < mUserResponses.size(); ++i) {
            if (mUserResponses.get(i).didUserProvidedAResponse()) {
                answeredQuestionsPositions.add(i + 1);
            }
        }

        return answeredQuestionsPositions;
    }

    public List<Integer> getUnAnsweredQuestions() {
        List<Integer> unAnsweredQuestionsPositions = new ArrayList<>();

        for (int i = 0; i < mUserResponses.size(); ++i) {
            if (!mUserResponses.get(i).didUserProvidedAResponse()) {
                unAnsweredQuestionsPositions.add(i + 1);
            }
        }

        return unAnsweredQuestionsPositions;
    }

    public List<String> getQuestionsIds() {
        List<String> questionsIds = new ArrayList<>();
        for (UserResponse userResponse : mUserResponses) {
            questionsIds.add(userResponse.getQuestionId());
        }
        return questionsIds;
    }

    public void setCurrentQuestionIndex(int currentQuestionIndex) {
        mCurrentQuestionIndex = currentQuestionIndex;
    }

    public int getCurrentQuestionIndex() {
        return mCurrentQuestionIndex;
    }

    public List<UserResponse> getUserResponses() {
        return mUserResponses;
    }

    public int getTotalQuestionsNumber() {
        return mTotalQuestionsNumber;
    }

    public int getCorrectRespondedQuestionsNumber() {
        int correctAnswers = 0;
        for (UserResponse userResponse : mUserResponses) {
            if (userResponse.isCorrect()) correctAnswers++;
        }

        return correctAnswers;
    }

    public long getDateTakingTheTest() {
        return dateTakingTheTest;
    }

    public long getElapsedTime() {
        return mElapsedTime;
    }
}
