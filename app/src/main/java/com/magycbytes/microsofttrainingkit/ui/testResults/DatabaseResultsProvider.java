package com.magycbytes.microsofttrainingkit.ui.testResults;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

/**
 * Created by Alex on 24/02/2017.
 */

public class DatabaseResultsProvider implements TestResultsProvider, LoaderManager.LoaderCallbacks<TestResultsViewModel> {

    private Events mListener;
    private Context mContext;
    private LoaderManager mLoader;

    public DatabaseResultsProvider(Context context, LoaderManager loader) {
        mContext = context;
        mLoader = loader;
    }

    @Override
    public void getResults() {
        mLoader.initLoader(3, null, this);
    }

    @Override
    public void setListener(Events listener) {
        mListener = listener;
    }

    @Override
    public Loader<TestResultsViewModel> onCreateLoader(int id, Bundle args) {
        return new AsyncDatabaseResultsLoader(mContext);
    }

    @Override
    public void onLoadFinished(Loader<TestResultsViewModel> loader, TestResultsViewModel data) {
        mListener.onResultsAvailable(data);
    }

    @Override
    public void onLoaderReset(Loader<TestResultsViewModel> loader) {

    }
}
