package com.magycbytes.microsofttrainingkit.ui.testQuestionReview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magycbytes.microsofttrainingkit.R;

import java.util.List;

/**
 * Created by Alex on 22/02/2017.
 */

class QuestionNumbersAdapter extends RecyclerView.Adapter<QuestionNumberViewHolder> {

    private List<Integer> mQuestionNumbers;
    private QuestionNumberViewHolder.Events mListener;

    QuestionNumbersAdapter(List<Integer> questionNumbers, QuestionNumberViewHolder.Events listener) {
        mQuestionNumbers = questionNumbers;
        mListener = listener;
    }

    @Override
    public QuestionNumberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_question_number, parent, false);
        return new QuestionNumberViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(QuestionNumberViewHolder holder, int position) {
        holder.show(mQuestionNumbers.get(position));
    }

    @Override
    public int getItemCount() {
        return mQuestionNumbers.size();
    }
}
