package com.magycbytes.microsofttrainingkit.ui.certificationMode.dataProvider;

import com.magycbytes.microsofttrainingkit.models.Question;
import com.magycbytes.microsofttrainingkit.ui.certificationMode.UserResponse;

import java.util.List;

/**
 * Created by sir Gregpry on 2/20/2017.
 */

public interface CertificationModeQuestionProvider {

    void setListener(Events listener);

    void getTestQuestions();

    interface Events {
        void onTestQuestionsAvailable(List<Question> questions, List<UserResponse> userResponses, long spentTime, int questionIndex);
    }
}
