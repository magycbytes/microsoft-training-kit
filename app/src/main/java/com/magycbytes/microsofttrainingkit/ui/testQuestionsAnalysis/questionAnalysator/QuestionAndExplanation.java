package com.magycbytes.microsofttrainingkit.ui.testQuestionsAnalysis.questionAnalysator;

/**
 * Created by sir Gregpry on 4/2/2017.
 */

public interface QuestionAndExplanation {

    void onShowExplanation(String explanation);
}
