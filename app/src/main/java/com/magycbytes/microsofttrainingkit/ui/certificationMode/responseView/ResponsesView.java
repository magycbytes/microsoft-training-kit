package com.magycbytes.microsofttrainingkit.ui.certificationMode.responseView;

import android.view.View;
import android.widget.LinearLayout;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.models.QuestionAnswer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sir Gregpry on 2/20/2017.
 */

public class ResponsesView implements IResponsesView {

    private LinearLayout mAnswersContainer;
    private ResponseContainer mResponseContainer;


    public ResponsesView(View view) {
        mAnswersContainer = (LinearLayout) view.findViewById(R.id.answersContainer);
    }

    @Override
    public void showResponsesWithSingleAnswers(List<QuestionAnswer> answers) {
        mAnswersContainer.removeAllViews();
        if (mResponseContainer != null) {
            mResponseContainer.clean();
        }

        mResponseContainer = new RadioButtonResponseContainer(mAnswersContainer.getContext());
        mResponseContainer.showResponses(answers, mAnswersContainer);
    }

    @Override
    public void showResponsesWithMultipleAnswers(List<QuestionAnswer> answers) {
        mAnswersContainer.removeAllViews();

        if (mResponseContainer != null) {
            mResponseContainer.clean();
        }

        mResponseContainer = new CheckBoxResponseContainer(mAnswersContainer.getContext());
        mResponseContainer.showResponses(answers, mAnswersContainer);
    }

    @Override
    public void removeAnswersOptions() {
        if (mResponseContainer != null) {
            mResponseContainer.removeUserSelection();
        }
    }

    @Override
    public void showUserResponse(List<String> responseIds) {
        if (mResponseContainer != null) {
            mResponseContainer.showUserResponseWithoutColoring(responseIds);
        }
    }

    @Override
    public List<String> getSelectedAnswerId() {
        if (mResponseContainer != null) {
            return mResponseContainer.getSelectedAnswerId();
        }
        return new ArrayList<>();
    }

    @Override
    public void showCorrectAnswers(List<String> correctIds) {
        if (mResponseContainer != null) {
            mResponseContainer.showCorrectAnswers(correctIds);
        }
    }
}
