package com.magycbytes.microsofttrainingkit.ui.testResults;

/**
 * Created by Alex on 24/02/2017.
 */

public class TestResultsPresenter implements TestResultsProvider.Events {

    private TestResultsProvider mProvider;
    private TestResultsView mView;

    public TestResultsPresenter(TestResultsProvider provider, TestResultsView view) {
        mProvider = provider;
        mView = view;

        mProvider.setListener(this);
    }

    public void showResults() {
        mProvider.getResults();
    }

    @Override
    public void onResultsAvailable(TestResultsViewModel viewModel) {
        mView.showResults(viewModel);
    }
}
