package com.magycbytes.microsofttrainingkit.ui.testResults;

import android.view.View;
import android.widget.TextView;

import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.customViews.ProgressBarStyled;

/**
 * Created by Alex on 22/02/2017.
 */

public class TestResultsView implements View.OnClickListener {

    private Events mListener;
    private TextView mDateTakeTest;
    private TextView mSpentTime;
    private TextView mUserScorePercents;
    private TextView mStatusTest;
    private TextView mUserScorePercentsAnalysis;
    private TextView mUserCorrectAnswers;
    private TextView mPassingScorePercents;
    private TextView mPassingScoreAnswers;
    private ProgressBarStyled mUserPercentsIndicator;

    TestResultsView(View view) {
        view.findViewById(R.id.exitTestButton).setOnClickListener(this);
        view.findViewById(R.id.retakeTestButton).setOnClickListener(this);
        view.findViewById(R.id.reviewAnswersButton).setOnClickListener(this);

        mDateTakeTest = (TextView) view.findViewById(R.id.takenTestDate);
        mSpentTime = (TextView) view.findViewById(R.id.spentTime);
        mUserScorePercents = (TextView) view.findViewById(R.id.userScorePercents);
        mStatusTest = (TextView) view.findViewById(R.id.statusText);
        mUserScorePercentsAnalysis = (TextView) view.findViewById(R.id.userScorePercentsText);
        mUserCorrectAnswers = (TextView) view.findViewById(R.id.userCorrectAnswers);
        mPassingScorePercents = (TextView) view.findViewById(R.id.passingScorePercents);
        mPassingScoreAnswers = (TextView) view.findViewById(R.id.passingCorrectAnswersCount);
        mUserPercentsIndicator = (ProgressBarStyled) view.findViewById(R.id.resultUserPercentsIndicator);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exitTestButton:
                mListener.onExitFromCurrentTest();
                break;
            case R.id.retakeTestButton:
                mListener.onRetakeTest();
                break;
            case R.id.reviewAnswersButton:
                mListener.onReviewQuestions();
                break;
        }
    }

    public void setListener(Events listener) {
        mListener = listener;
    }

    public void showResults(TestResultsViewModel viewModel) {
        mDateTakeTest.setText(viewModel.getDateTaking());
        mSpentTime.setText(viewModel.getTookTime());
        mUserScorePercents.setText(viewModel.getScorePercents());
        mStatusTest.setText(viewModel.getPassingStatus());
        mUserScorePercentsAnalysis.setText(viewModel.getScorePercents());
        mUserCorrectAnswers.setText(viewModel.getCorrectAnsweredQuestions());
        mPassingScorePercents.setText(viewModel.getPassingPercents());
        mPassingScoreAnswers.setText(viewModel.getPassingQuestionsNumber());
        mUserPercentsIndicator.showProgress(viewModel.getScorePercentsInt());
    }

    interface Events {
        void onExitFromCurrentTest();

        void onRetakeTest();

        void onReviewQuestions();
    }
}
