package com.magycbytes.microsofttrainingkit;

import com.google.gson.Gson;
import com.magycbytes.microsofttrainingkit.models.SubObjective;
import com.magycbytes.microsofttrainingkit.models.TrainingContent;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sir Gregpry on 2/19/2017.
 */

public class SubObjectivesCompartiment extends CompartmentParser {

    private List<SubObjective> mSubObjectives = new ArrayList<>();

    public SubObjectivesCompartiment(BufferedReader bufferedReader) {
        super(bufferedReader);
    }

    @Override
    public String getSerializedResponse() {
        return new Gson().toJson(mSubObjectives);
    }

    @Override
    public void saveLineValue(String fileLine) {
        String[] values = getSplitValues();

        SubObjective subObjective = new SubObjective();
        subObjective.setId(values[0]);
        subObjective.setName(values[1]);
        subObjective.setObjectiveId(values[2]);

        mSubObjectives.add(subObjective);
    }

    @Override
    public void visit(TrainingContent trainingContent) {
        trainingContent.setSubObjectives(mSubObjectives);
    }
}
