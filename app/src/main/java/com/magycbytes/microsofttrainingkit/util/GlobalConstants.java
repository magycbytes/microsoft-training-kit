package com.magycbytes.microsofttrainingkit.util;

/**
 * Created by Alex on 01/03/2017.
 */

public class GlobalConstants {

    public static final String REMAINING_TIME_FORMAT = "%02d:%02d";
}
