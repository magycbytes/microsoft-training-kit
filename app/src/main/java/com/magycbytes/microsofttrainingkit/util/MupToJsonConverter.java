package com.magycbytes.microsofttrainingkit.util;

import android.content.Context;

import com.google.gson.Gson;
import com.magycbytes.microsofttrainingkit.CompartmentParser;
import com.magycbytes.microsofttrainingkit.NextKeysCompartment;
import com.magycbytes.microsofttrainingkit.ObjectivesCompartment;
import com.magycbytes.microsofttrainingkit.QuestionCompartment;
import com.magycbytes.microsofttrainingkit.R;
import com.magycbytes.microsofttrainingkit.ReferencesCompartment;
import com.magycbytes.microsofttrainingkit.SubObjectivesCompartiment;
import com.magycbytes.microsofttrainingkit.TestCompartment;
import com.magycbytes.microsofttrainingkit.models.TrainingContent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by sir Gregpry on 2/22/2017.
 */

public class MupToJsonConverter {

    public static String convert(Context context) {
        try {
            return parseFile(context);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String parseFile(Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.test_data)));
        String line = reader.readLine();
        TrainingContent trainingContent = new TrainingContent();
        while (line != null) {
            if (line.startsWith("[")) {
                CompartmentParser parser = getParser(line, reader);
                if (parser == null) break;

                parser.parse();
                parser.visit(trainingContent);
                line = parser.getCurrentLine();
            }
        }
        return new Gson().toJson(trainingContent);
    }

    private static CompartmentParser getParser(String line, BufferedReader bufferedReader) {
        switch (line) {
            case "[NextKeys]":
                return new NextKeysCompartment(bufferedReader);
            case "[Test]":
                return new TestCompartment(bufferedReader);
            case "[Objectives]":
                return new ObjectivesCompartment(bufferedReader);
            case "[SubObjectives]":
                return new SubObjectivesCompartiment(bufferedReader);
            case "[ReferenceProducts]":
                return new ReferencesCompartment(bufferedReader);
            case "[Questions]":
                return new QuestionCompartment(bufferedReader);
            default:
                return null;
        }
    }
}
