package com.magycbytes.microsofttrainingkit.util;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by sir Gregpry on 2/22/2017.
 */

public class ExamDatabase {

    private static final String LOG_TAG = ExamDatabase.class.getSimpleName();

    public static final String USER_RESPONSE = "UserResponse";

    public static void saveTestSession(Context context, String userResponses) {
        Log.d(LOG_TAG, "Saving test session: ");
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(USER_RESPONSE, userResponses)
                .apply();
    }

    public static String getUserResponses(Context context) {
        String serializedResponses = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(USER_RESPONSE, null);
        Log.d(LOG_TAG, "Serialized user response: ");
        return serializedResponses;
    }

    public static void resetLastSession(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(USER_RESPONSE, "")
                .apply();
    }

    public static boolean isPreviousSessionAvailable(Context context) {
        String userResponses = getUserResponses(context);
        return userResponses != null && !userResponses.isEmpty();
    }
}
