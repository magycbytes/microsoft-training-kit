package com.magycbytes.microsofttrainingkit;

import com.magycbytes.microsofttrainingkit.models.TrainingContent;

import java.io.BufferedReader;

/**
 * Created by sir Gregory on 2/9/2017.
 */

public class TestCompartment extends CompartmentParser {

    public TestCompartment(BufferedReader bufferedReader) {
        super(bufferedReader);
    }

    @Override
    public String getSerializedResponse() {
        return null;
    }

    @Override
    public void saveLineValue(String fileLine) {
        switch (fileLine) {

        }
    }

    @Override
    public void visit(TrainingContent trainingContent) {

    }
}
