package com.magycbytes.microsofttrainingkit;

import com.google.gson.Gson;
import com.magycbytes.microsofttrainingkit.models.ExamStatistics;
import com.magycbytes.microsofttrainingkit.models.TrainingContent;

import java.io.BufferedReader;

/**
 * Created by sir Gregory on 2/9/2017.
 */

public class NextKeysCompartment extends CompartmentParser {

    private ExamStatistics mExamStatistics = new ExamStatistics();

    public NextKeysCompartment(BufferedReader bufferedReader) {
        super(bufferedReader);
    }

    @Override
    public String getSerializedResponse() {
        return new Gson().toJson(mExamStatistics);
    }

    @Override
    public void saveLineValue(String fileLine) {
        switch (fileLine) {
            case "ReferenceProduct":
                mExamStatistics.setReferenceProducts(Integer.valueOf(mCurrentValue));
                break;
            case "References":
                mExamStatistics.setReferences(Integer.valueOf(mCurrentValue));
                break;
            case "Objectives":
                mExamStatistics.setObjectives(Integer.valueOf(mCurrentValue));
                break;
            case "SubObjectives":
                mExamStatistics.setSubObjectives(Integer.valueOf(mCurrentValue));
                break;
            case "Questions":
                mExamStatistics.setQuestions(Integer.valueOf(mCurrentValue));
                break;
            case "Answers":
                mExamStatistics.setAnswers(Integer.valueOf(mCurrentValue));
                break;
            case "QuestionReferenceLinks":
                mExamStatistics.setQuestionsReferenceLinks(Integer.valueOf(mCurrentValue));
                break;
        }
    }

    @Override
    public void visit(TrainingContent trainingContent) {

    }
}
