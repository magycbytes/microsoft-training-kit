package com.magycbytes.microsofttrainingkit;

import com.magycbytes.microsofttrainingkit.models.QuestionAnswer;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by sir Gregpry on 2/19/2017.
 */

public class QuestionAnswerXmlDeserializator {

    public static List<QuestionAnswer> deserialize(String originalText) {
        List<QuestionAnswer> deserializedAnswers = new ArrayList<>();

        originalText = originalText.substring(originalText.indexOf("<answer>"));
        String[] answers = originalText.split(Pattern.quote("</answer>"));

        for (String answer : answers) {
            if (answer.equals("</answers>")) continue;

            QuestionAnswer questionAnswer = new QuestionAnswer();
            questionAnswer.setId(getAnswerId(answer));
            questionAnswer.setText(getText(answer));
            questionAnswer.setScore(getScore(answer));
            questionAnswer.setCorrect(isCorrect(answer));

            deserializedAnswers.add(questionAnswer);
        }

        return deserializedAnswers;
    }

    private static String getAnswerId(String answer) {
        return getTagValue("answerid", answer);
    }

    private static String getText(String answer) {
        return getTagValue("mattext", answer);
    }

    private static int getScore(String answer) {
        String score = getTagValue("score", answer);
        return Integer.parseInt(score);
    }

    private static Boolean isCorrect(String answer) {
        String flagValue = getTagValue("iscorrect", answer);
        return flagValue.equals("True");
    }

    private static String getTagValue(String valueName, String originText) {
        String startTag = "<" + valueName + ">";
        String endTag = "</" + valueName + ">";
        int startIndex = originText.indexOf(startTag) + startTag.length();
        int endIndex = originText.indexOf(endTag);

        return originText.substring(startIndex, endIndex);
    }
}
